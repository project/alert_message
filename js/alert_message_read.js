/**
 * @file
 * Alert message read.
 */
((Drupal) => {
  Drupal.behaviors.alert_message_read = {
    attach(context) {
      const elements = context.querySelectorAll('.alert-message-close');
      elements.forEach((element) => {
        element.addEventListener(
          'click',
          () => {
            const messageId = element.getAttribute('data-message-id');
            let data = [];
            const cookie = document.cookie
              .split('; ')
              .find((row) => row.startsWith('alertMessageClosed='));
            if (cookie) {
              data = JSON.parse(cookie.split('=')[1]);
            }
            data.push(messageId);
            document.cookie = `alertMessageClosed=${JSON.stringify(data)}; path=/`;
            const message = document.getElementById(
              `alert-message-${messageId}`,
            );
            message.remove();
          },
          { once: true },
        );
      });
    },
  };
})(Drupal);
