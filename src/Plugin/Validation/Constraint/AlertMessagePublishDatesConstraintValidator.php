<?php

namespace Drupal\alert_message\Plugin\Validation\Constraint;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that the unpublish date is not earlier to publish one.
 *
 * @package alert_message
 */
class AlertMessagePublishDatesConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Class constructor.
   */
  public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\alert_message\AlertMessageInterface $items */
    $publish_date = $items->getPublishDate()->getTimestamp();
    $unpublish_date = $items->getUnPublishDate()->getTimestamp();
    if ($unpublish_date <= $this->time->getRequestTime()) {
      $this->context->addViolation($constraint->unpublishDateIsPast);
    }
    if ($unpublish_date <= $publish_date) {
      $this->context->addViolation($constraint->unpublishEarlierPublish);
    }
  }

}
