<?php

namespace Drupal\alert_message\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Unpublish date earlier than the publish one.
 *
 * @Constraint(
 *  id = "AlertMessagePublishDates",
 *  label = @Translation("Constraints for publishing options of alert message.", context="Validation")
 * )
 */
class AlertMessagePublishDatesConstraint extends Constraint {

  /**
   * {@inheritdoc}
   */
  public $unpublishDateIsPast = "The unpublish date is in the past.";

  /**
   * {@inheritdoc}
   */
  public $unpublishEarlierPublish = "The unpublish date is earlier than the publishing one.";

}
