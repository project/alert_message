<?php

namespace Drupal\alert_message;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a alert message entity type.
 */
interface AlertMessageInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Get the message to be displayed.
   *
   * @return string
   *   The message.
   */
  public function getMessage();

  /**
   * Set the message to be displayed.
   *
   * @param string $alert_message
   *   An array of formatted message.
   *
   * @return $this
   */
  public function setMessage(string $alert_message);

  /**
   * Set targeted users.
   *
   * @param \Drupal\user\Entity\User[] $users
   *   The users.
   *
   * @return $this
   */
  public function setTargetedUsers(array $users);

  /**
   * Get targeted users.
   *
   * @return \Drupal\user\Entity\User[]
   *   The users.
   */
  public function getTargetedUsers();

  /**
   * Set user ids.
   *
   * @param array $user_ids
   *   An array of user ids.
   *
   * @return $this
   */
  public function setTargetedUserIds(array $user_ids);

  /**
   * Get targeted user ids.
   *
   * @return array
   *   The user ids.
   */
  public function getTargetedUserIds(): array;

  /**
   * Get targeted roles.
   *
   * @param \Drupal\user\Entity\Role[] $roles
   *   The user roles.
   *
   * @return $this
   */
  public function setTargetedRoles(array $roles);

  /**
   * Get targeted roles.
   *
   * @return \Drupal\user\Entity\Role[]
   *   The user roles.
   */
  public function getTargetedRoles();

  /**
   * Get targeted role ids.
   *
   * @return array
   *   An array of role ids.
   */
  public function getTargetedRoleIds(): array;

  /**
   * Set targeted role ids.
   *
   * @param array $user_ids
   *   An array of role ids.
   *
   * @return $this
   */
  public function setTargetedRoleIds(array $user_ids);

  /**
   * Get the publish time.
   *
   * @param string $timezone
   *   The timezone.
   *
   * @return \Drupal\core\Datetime\DrupalDateTime
   *   The publish date.
   */
  public function getPublishDate($timezone = 'UTC'): DrupalDateTime;

  /**
   * Set the publish date.
   *
   * @param \Drupal\core\Datetime\DrupalDateTime $publish_date
   *   The publish date.
   *
   * @return $this
   */
  public function setPublishDate(DrupalDateTime $publish_date);

  /**
   * Get the unpublish time.
   *
   * @param string $timezone
   *   The timezone.
   *
   * @return \Drupal\core\Datetime\DrupalDateTime
   *   The unpublish date.
   */
  public function getUnpublishDate($timezone = 'UTC'): DrupalDateTime;

  /**
   * Set the unpublish date.
   *
   * @param \Drupal\core\Datetime\DrupalDateTime $unpublish_date
   *   The publish date.
   *
   * @return $this
   */
  public function setUnpublishDate(DrupalDateTime $unpublish_date);

  /**
   * Get whether the message needs to be published.
   *
   * @return bool
   *   Whether the message needs to be published.
   */
  public function getToPublish(): bool;

  /**
   * Set whether the message needs to be published.
   *
   * @param bool $to_publish
   *   Whether the message needs to be published.
   *
   * @return $this
   */
  public function setToPublish(bool $to_publish);

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   Creation timestamp of the message.
   */
  public function getCreatedTime();

  /**
   * Sets the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the message status.
   *
   * @return bool
   *   The status.
   */
  public function getStatus(): bool;

  /**
   * Set the message status.
   *
   * @param bool $status
   *   The message status.
   *
   * @return $this
   */
  public function setStatus(bool $status);

}
