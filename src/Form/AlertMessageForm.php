<?php

namespace Drupal\alert_message\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the alert message entity edit forms.
 */
class AlertMessageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $form_state->setRedirect('entity.alert_message.canonical', ['alert_message' => $entity->id()]);

    return $result;
  }

}
