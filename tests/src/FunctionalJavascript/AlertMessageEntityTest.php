<?php

namespace Drupal\Tests\alert_message\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests alert message entity behaviors.
 *
 * @group alert_message
 */
class AlertMessageEntityTest extends WebDriverTestBase {

  /**
   * Set default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'alert_message',
  ];

  /**
   * The assertion service.
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected $webAssert;

  /**
   * The adminUser.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * The firstUser.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $firstUser;

  /**
   * The secondUser.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $secondUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->webAssert = $this->assertSession();

    $this->adminUser = $this->drupalCreateUser([
      'view the administration theme',
      'access administration pages',
      'access alert_message overview',
      'administer alert message',
      'create alert_message',
      'update any alert_message',
    ], NULL, TRUE);

    $this->drupalCreateRole([], 'alert_message', 'Alert message');

    $this->firstUser = $this->drupalCreateUser();

    $this->secondUser = $this->drupalCreateUser();

    // Add alert message block.
    $this->drupalPlaceBlock('alert_message', ['region' => 'highlighted']);
  }

  /**
   * Tests alert global message.
   */
  public function testGlobalAlertMessage() {
    $this->drupalLogin($this->adminUser);
    // Create a published global message.
    $this->drupalGet('alert-message/add');
    sleep(1);
    $this->submitForm([
      'label[0][value]' => 'Test global message',
      'message[0][value]' => 'Content of global test message',
    ], 'Save');

    $this->webAssert->addressEquals('alert-message/1');

    // Go to front and check message is there.
    $this->drupalGet('<front>');
    $this->webAssert->pageTextContains('Content of global test message');
    // Dismiss alert message.
    $this->click('.alert-message-close');
    $this->webAssert->pageTextNotContains('Content of global test message');

    // Reload and validate alert is not viewable.
    $this->getSession()->reload();
    $this->webAssert->pageTextNotContains('Content of global test message');

    // Check alert message as anonymous.
    $this->getSession()->setCookie('alertMessageClosed');
    $this->drupalLogout();

    // Go to front and check message is there.
    $this->drupalGet('<front>');
    $this->webAssert->pageTextContains('Content of global test message');

    // Dismiss alert message.
    $this->click('.alert-message-close');
    $this->webAssert->pageTextNotContains('Content of global test message');

    // Reload and validate alert is not viewable.
    $this->getSession()->reload();
    $this->webAssert->pageTextNotContains('Content of global test message');
  }

  /**
   * Tests alert user message.
   */
  public function testUserAlertMessage() {
    $this->drupalLogin($this->adminUser);
    // Create a published user message.
    $this->drupalGet('alert-message/add');
    sleep(1);
    $this->submitForm([
      'label[0][value]' => 'Test user message',
      'message[0][value]' => 'Content of user test message',
      'users[target_id]' => "{$this->firstUser->getAccountName()} ({$this->firstUser->id()})",
    ], 'Save');

    // As an anonymous, no message.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of user test message');

    // As second user, no message.
    $this->drupalLogin($this->secondUser);
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of user test message');

    // As first user I can see message.
    $this->drupalLogin($this->firstUser);
    $this->drupalGet('<front>');
    $this->webAssert->pageTextContains('Content of user test message');
    $this->click('.alert-message-close');
    $this->getSession()->reload();
    $this->webAssert->pageTextNotContains('Content of user test message');
  }

  /**
   * Tests alert user message.
   */
  public function testRoleAlertMessage() {
    $this->drupalLogin($this->adminUser);
    // Create a published user message.
    $this->drupalGet('alert-message/add');
    sleep(1);
    $this->submitForm([
      'label[0][value]' => 'Test role message',
      'message[0][value]' => 'Content of role test message',
      'roles[]' => 'alert_message',
    ], 'Save');

    // As an anonymous, no message.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of role test message');

    // As first user, no message.
    $this->drupalLogin($this->firstUser);
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of role test message');

    // When user has role, they can see message.
    $this->firstUser->addRole('alert_message');
    $this->firstUser->save();
    $this->getSession()->reload();
    $this->webAssert->pageTextContains('Content of role test message');
  }

  /**
   * Tests alert user message.
   */
  public function testUserAndRoleAlertMessage() {
    $this->drupalLogin($this->adminUser);
    // Create a published user message.
    $this->drupalGet('alert-message/add');
    sleep(1);
    $this->submitForm([
      'label[0][value]' => 'Test user and role message',
      'message[0][value]' => 'Content of user and role test message',
      'users[target_id]' => "{$this->firstUser->getAccountName()} ({$this->firstUser->id()})",
      'roles[]' => 'alert_message',
    ], 'Save');

    // As an anonymous, no message.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of user and role test message');

    // As first user, no message.
    $this->drupalLogin($this->firstUser);
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of user and role test message');

    // When user has role, they can see message.
    $this->firstUser->addRole('alert_message');
    $this->firstUser->save();
    $this->getSession()->reload();
    $this->webAssert->pageTextContains('Content of user and role test message');

    // Second user with role can't see message.
    $this->drupalLogin($this->secondUser);
    $this->secondUser->addRole('alert_message');
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of user and role test message');
  }

}
