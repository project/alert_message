<?php

namespace Drupal\Tests\alert_message\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests alert message constraints.
 *
 * @group alert_message
 */
class AlertMessageConstraintsTest extends BrowserTestBase {

  /**
   * Set default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'alert_message',
  ];

  /**
   * The assertion service.
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected $webAssert;

  /**
   * The adminUser.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * The request time.
   *
   * @var int
   */
  protected $requestTime;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->webAssert = $this->assertSession();

    $this->adminUser = $this->drupalCreateUser([
      'view the administration theme',
      'access administration pages',
      'access alert_message overview',
      'administer alert message',
      'create alert_message',
      'update any alert_message',
    ], NULL, TRUE);

    $this->requestTime = $this->container->get('datetime.time')->getRequestTime();

    $this->dateFormatter = $this->container->get('date.formatter');

    // Add alert message block.
    $this->drupalPlaceBlock('alert_message', ['region' => 'highlighted']);
  }

  /**
   * Tests alert message scheduling.
   */
  public function testConstraints() {
    $this->drupalLogin($this->adminUser);
    // Create a published global message.
    $this->drupalGet('alert-message/add');
    $this->submitForm([
      'label[0][value]' => 'Test global message',
      'message[0][value]' => 'Content of global test message',
      'publish_date[0][value][date]' => $this->dateFormatter->format($this->requestTime, 'custom', 'Y-m-d'),
      'publish_date[0][value][time]' => $this->dateFormatter->format($this->requestTime, 'custom', 'H:i:s'),
      'unpublish_date[0][value][date]' => $this->dateFormatter->format($this->requestTime - 3600, 'custom', 'Y-m-d'),
      'unpublish_date[0][value][time]' => $this->dateFormatter->format($this->requestTime - 3600, 'custom', 'H:i:s'),
    ], 'Save');

    $this->webAssert->pageTextContains('The unpublish date is in the past.');

    $this->submitForm([
      'label[0][value]' => 'Test global message',
      'message[0][value]' => 'Content of global test message',
      'publish_date[0][value][date]' => $this->dateFormatter->format($this->requestTime + 7200, 'custom', 'Y-m-d'),
      'publish_date[0][value][time]' => $this->dateFormatter->format($this->requestTime + 7200, 'custom', 'H:i:s'),
      'unpublish_date[0][value][date]' => $this->dateFormatter->format($this->requestTime + 3600, 'custom', 'Y-m-d'),
      'unpublish_date[0][value][time]' => $this->dateFormatter->format($this->requestTime + 3600, 'custom', 'H:i:s'),
    ], 'Save');

    $this->webAssert->pageTextContains('The unpublish date is earlier than the publishing one.');
  }

}
