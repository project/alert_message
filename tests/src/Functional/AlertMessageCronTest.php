<?php

namespace Drupal\Tests\alert_message\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Tests alert message cron.
 *
 * @group alert_message
 */
class AlertMessageCronTest extends BrowserTestBase {

  use CronRunTrait;

  /**
   * Set default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'alert_message',
  ];

  /**
   * The assertion service.
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected $webAssert;

  /**
   * The adminUser.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * The alert message storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $alertMessageStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->webAssert = $this->assertSession();

    $this->adminUser = $this->drupalCreateUser([
      'view the administration theme',
      'access administration pages',
      'access alert_message overview',
      'administer alert message',
      'create alert_message',
      'update any alert_message',
    ], NULL, TRUE);

    $this->alertMessageStorage = $this->container->get('entity_type.manager')->getStorage('alert_message');

    $this->dateFormatter = $this->container->get('date.formatter');

    // Add alert message block.
    $this->drupalPlaceBlock('alert_message', ['region' => 'highlighted']);
  }

  /**
   * Tests alert message scheduling.
   */
  public function testPublishingAndUnpublishing() {
    $this->drupalLogin($this->adminUser);
    // Create a published global message.
    $this->drupalGet('alert-message/add');
    $this->submitForm([
      'label[0][value]' => 'Test global message',
      'message[0][value]' => 'Content of global test message',
      'publish_date[0][value][date]' => $this->dateFormatter->format(time() + 5, 'custom', 'Y-m-d'),
      'publish_date[0][value][time]' => $this->dateFormatter->format(time() + 5, 'custom', 'H:i:s'),
      'unpublish_date[0][value][date]' => $this->dateFormatter->format(time() + 10, 'custom', 'Y-m-d'),
      'unpublish_date[0][value][time]' => $this->dateFormatter->format(time() + 10, 'custom', 'H:i:s'),
    ], 'Save');

    $alert_messages = $this->alertMessageStorage->loadMultiple();
    /** @var \Drupal\alert_message\Entity\AlertMessage $alert_message */
    $alert_message = reset($alert_messages);
    $this->assertTrue($alert_message->getToPublish());
    $this->assertFalse($alert_message->getStatus());

    // The alert message shouldn't be displayed.
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of global test message');

    // Publishing action.
    sleep(6);
    $this->cronRun();
    $this->alertMessageStorage->resetCache([$alert_message->id()]);
    /** @var \Drupal\alert_message\Entity\AlertMessage $alert_message */
    $alert_message = $this->alertMessageStorage->load($alert_message->id());
    $this->assertFalse($alert_message->getToPublish());
    $this->assertTrue($alert_message->getStatus());
    $this->drupalGet('<front>');
    $this->webAssert->pageTextContains('Content of global test message');

    // Unpublishing action.
    sleep(5);
    $this->cronRun();
    $this->alertMessageStorage->resetCache([$alert_message->id()]);
    /** @var \Drupal\alert_message\Entity\AlertMessage $alert_message */
    $alert_message = $this->alertMessageStorage->load($alert_message->id());
    $this->assertFalse($alert_message->getToPublish());
    $this->assertFalse($alert_message->getStatus());
    $this->drupalGet('<front>');
    $this->webAssert->pageTextNotContains('Content of global test message');
  }

}
