# Alert Message
A lightweight Drupal module for displaying alert messages.

| Feature | Description                               |
| ---      | ---                                        |
| Administration | An intuitive UI for managing alert messages                            |
| Roles/Names | Configures alert messages to target specific users and user roles |
| Layout    | Use configurable templates to change the look & feel             |
| Internationalization | Fully translatable                                   |

# Installation

Install the module with composer.

```bash
  composer require drupal/alert_message
```

# Usage

- Enable the module
- Insert the alert message block in /admin/structure/block
- Create your first alert message in /admin/content/alert-message
