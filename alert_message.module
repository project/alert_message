<?php

/**
 * @file
 * Provides a alert message entity type.
 */

use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Implements hook_help().
 */
function alert_message_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.alert_message':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module provides an easy way to display messages to users of the website. These messages can be dismissed per session, by ticking a close button.') . '</p>';
      $output .= '<p>' . t('You eventually can target specific user roles or users when creating a message. You can also decide at which date it should be published and unpublished (with a dependency to the cron though).') . '</p>';
      $output .= '<h3>' . t('Requirements') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . t('Enable the alert message block in the <a href="@url">admin block structure</a>', ['@url' => Url::fromRoute('block.admin_display')->toString()]) . '</li>';
      $output .= '<li>' . t('Create a alert message in the <a href="@url">administration interface</a>', ['@url' => Url::fromRoute('entity.alert_message.settings')->toString()]) . '</li>';
      $output .= '</ul>';
      return $output;
  }
}

/**
 * Implements hook_theme().
 */
function alert_message_theme() {
  return [
    'alert_message' => [
      'render element' => 'elements',
    ],
    'block__alert_messages' => [
      'variables' => [
        'alert_messages' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_cron().
 */
function alert_message_cron() {
  $alert_message_storage = \Drupal::entityTypeManager()->getStorage('alert_message');
  // Publish/Unpublish any awaiting message.
  $alert_message_query = $alert_message_storage->getQuery('OR');
  $alert_message_query->accessCheck(FALSE);
  $alert_message_ids = $alert_message_query
    ->condition('to_publish', TRUE)
    ->condition('status', TRUE)
    ->execute();
  /** @var \Drupal\alert_message\Entity\AlertMessage $alert_message */
  foreach ($alert_message_storage->loadMultiple($alert_message_ids) as $alert_message) {
    $alert_message->save();
  }
}

/**
 * Prepares variables for alert message templates.
 *
 * Default template: alert-message.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing
 *     the alert message information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_alert_message(array &$variables) {
  $entity = $variables['elements']['#alert_message'];
  // Early return if message has been read.
  $cookie_alert_messages = \Drupal::request()->cookies->get('alertMessageClosed');
  if ($cookie_alert_messages && in_array($entity->id(), Json::decode($cookie_alert_messages))) {
    $variables['message_read'] = TRUE;
    return;
  }
  // Pass necessary variables to template.
  $variables['attributes'] = new Attribute([
    'id' => 'alert-message-' . $entity->id(),
    'class' => ['alert-message'],
  ]);
  $variables['#attached']['library'][] = 'alert_message/read';
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
  $variables['message_id'] = $entity->id();
}

/**
 * Implements hook_user_cancel().
 */
function alert_message_user_cancel($edit, UserInterface $account, $method) {
  switch ($method) {
    case 'user_cancel_block_unpublish':
      // Unpublish alert messages.
      $storage = \Drupal::entityTypeManager()->getStorage('alert_message');
      $alert_message_ids = $storage->getQuery()
        ->accessCheck(FALSE)
        ->condition('uid', $account->id())
        ->condition('status', 1)
        ->execute();
      foreach ($storage->loadMultiple($alert_message_ids) as $alert_message) {
        $alert_message->set('status', FALSE);
        $alert_message->save();
      }
      break;

    case 'user_cancel_reassign':
      // Anonymize alert messages.
      $storage = \Drupal::entityTypeManager()->getStorage('alert_message');
      $alert_message_ids = $storage->getQuery()
        ->accessCheck(FALSE)
        ->condition('uid', $account->id())
        ->execute();
      foreach ($storage->loadMultiple($alert_message_ids) as $alert_message) {
        $alert_message->setOwnerId(0);
        $alert_message->save();
      }
      break;
  }
}

/**
 * Implements hook_ENTITY_TYPE_predelete() for user entities.
 */
function alert_message_user_predelete(UserInterface $account) {
  // Delete alert messages.
  $storage = \Drupal::entityTypeManager()->getStorage('alert_message');
  $alert_message_ids = $storage->getQuery()
    ->accessCheck(FALSE)
    ->condition('uid', $account->id())
    ->execute();
  $alert_messages = $storage->loadMultiple($alert_message_ids);
  $storage->delete($alert_messages);
}
